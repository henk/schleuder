schleuder (4.0.3-6) unstable; urgency=medium

  * debian/control:
    - Bump Standards-Version to 4.6.1, no changes required.
    - Drop obsolete field XB-Ruby-Versions.
  * debian/patches:
    - Add patch to ensure correct Ruby-version is used, if calling daemons in
      specs. (Closes: #1019682)
  * debian/ruby-tests.rake:
    - Use correct Ruby-version if calling rake. (Closes: #1019682)

 -- Georg Faerber <georg@debian.org>  Mon, 31 Oct 2022 17:04:23 +0000

schleuder (4.0.3-5) unstable; urgency=medium

  * debian/patches:
    - Add patch to run testsuite with UTC as the timezone.
    - Add patch to enable full backtraces if specs are failing.

 -- Georg Faerber <georg@debian.org>  Thu, 27 Oct 2022 10:47:52 +0000

schleuder (4.0.3-4) unstable; urgency=medium

  * debian/patches:
    - Add patch to respect existing 'LOCAL_IP_ADDR' environment variable.
    - Add patch to relax expected error message in specs, if refreshing keys
      from a keyserver fails.
  * debian/ruby-tests.rake:
    - Set 'LOCAL_IP_ADDR' environment variable to 'localhost'.

 -- Georg Faerber <georg@debian.org>  Sat, 16 Apr 2022 21:48:30 +0000

schleuder (4.0.3-3) unstable; urgency=medium

  * debian/patches:
    - Drop patch, introduced in 4.0.3-2, to relax expected GPG key dates.
    - Instead, add patch to enforce a specific timezone and locale. This
      approach is preferred, as it makes dealing with the testsuite easier for
      humans, but requires real-world exposure during reproducible builds to
      check if this works as expected.

 -- Georg Faerber <georg@debian.org>  Wed, 13 Apr 2022 13:29:30 +0000

schleuder (4.0.3-2) unstable; urgency=medium

  * debian/patches:
    - Add patch to relax the expected creation and expiration dates of GPG
      keys. Specs which rely on hardcoded dates will fail during reproducible
      builds, as such builds rely on timezone-variations, and these dates are,
      in fact, timezone-sensitive.

 -- Georg Faerber <georg@debian.org>  Wed, 13 Apr 2022 10:19:36 +0000

schleuder (4.0.3-1) unstable; urgency=medium

  * New upstream version.
  * debian/patches:
    - Drop obsolete patches integrated upstream.
    - Refresh remaining patches.
    - Add patch to ensure IPv4 or IPv6 address for the SKS mock server to bind
      to if called in specs.
  * debian/ruby-tests.rake:
    - Set environment variable to drop usage of 'byebug' in specs.

 -- Georg Faerber <georg@debian.org>  Tue, 12 Apr 2022 23:14:25 +0000

schleuder (4.0.2-1) unstable; urgency=medium

  * New upstream version. (Closes: #991662, #998484)
  * debian/control:
    - (Build-)Depend on ruby-bcrypt.
    - Update description in regards to version 4.
    - Bump Standards-Version to 4.6.0, no changes required.
  * debian/patches:
    - Add patch to drop usage of 'byebug' in specs.
    - Drop obsolete patches integrated upstream.
    - Refresh remaining patches.
  * debian/ruby-tests.rake:
    - Enable tests against Ruby 3.0, again.
  * debian/schleuder.postinst:
    - Improve handling of initial database setup and migrations, and TLS
      certificate and key required to serve the API.
    - Create directories to store user-provided code.
  * debian/schleuder.postrm:
    - Remove directories to store user-provided code, if empty.

 -- Georg Faerber <georg@debian.org>  Wed, 06 Apr 2022 16:58:51 +0000

schleuder (3.6.0-4) unstable; urgency=medium

  * debian/control: Drop depending on ruby-interpreter, according to team
    standards.
  * debian/patches: Pull in upstream patch to migrate boolean values to
    integers, if the ActiveRecord SQLite3 connection adapter is in use.
    Since ActiveRecord >= 6.0, the relevant code relies on boolean
    serialization to use 1 and 0, but does not natively recognize 't' and 'f'
    as booleans were previously serialized. (Closes: #1002622)
  * debian/ruby-tests.rake: Skip tests if running Ruby 3.0 as some tests are
    currently not compatible. This will be reverted soon, once Schleuder 4 is
    to be uploaded.
  * debian/upstream/metadata: Provide file pointing to upstream resources.

 -- Georg Faerber <georg@debian.org>  Sat, 25 Dec 2021 20:19:26 +0000

schleuder (3.6.0-3) unstable; urgency=medium

  * debian/patches:
    - Pull in upstream patch to fix verifying encapsulated messages:
      monkeypatch ruby-mail-gpg to restore the use of the unaltered raw source
      of the relevant email part.
      This code was removed in a recent release of ruby-mail-gpg for yet
      unknown reasons, and uploaded to Debian via 0.4.4-1.

 -- Georg Faerber <georg@debian.org>  Thu, 29 Jul 2021 20:36:52 +0000

schleuder (3.6.0-2) unstable; urgency=medium

  * debian/control:
    - Drop obsolete version constraint of ruby-mail-gpg.
  * debian/patches:
    - Pull in upstream patch to handle GPGME::Error::Canceled.
    - Pull in upstream patch to fix preconditions of specs: Some expectations
      only worked as long as the content-transfer-encoding was not Base64.
      Some only worked if it was Quoted-Printable. Now they don't depend on
      the CTE anymore.
    - Pull in upstream patch to relax version of mail-gpg.
    - Refresh remaining patches to handle fuzz, introduced by these new
      patches.

 -- Georg Faerber <georg@debian.org>  Sun, 23 May 2021 17:32:19 +0000

schleuder (3.6.0-1.1) unstable; urgency=medium

  * Non-maintainer upload
  * improve hack to block passphrase interaction (Closes: #982627)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 12 Feb 2021 11:55:30 -0500

schleuder (3.6.0-1) unstable; urgency=medium

  * New upstream version.
  * debian/patches:
    - Drop patch to fix email address regex, merged upstream.
    - Refresh remaining patches.

 -- Georg Faerber <georg@debian.org>  Sun, 07 Feb 2021 18:14:33 +0000

schleuder (3.5.3-3) unstable; urgency=medium

  * debian/patches:
    - Add patch to relax thor version in gemspec. (Closes: #980578)
    - Add patch to pull in changes made upstream to fix email address regex.
    - Refresh remaining patches.

 -- Georg Faerber <georg@debian.org>  Tue, 02 Feb 2021 12:41:30 +0000

schleuder (3.5.3-2) unstable; urgency=low

  * Team upload.
  * debian/.gitattributes:
    - Add to keep unwanted files out of the source package.
  * debian/.gitlab-ci.yml:
    - Drop, obsolete.
  * debian/changelog:
    - Trim trailing whitespace.
  * debian/control:
    - Bump debhelper compat level to 13.
    - Bump Standards-Version to 4.5.1, no changes required.
    - Do not recommend haveged anymore. For details and discussion, see the
      related bug report. (Closes: #960066)
    - Drop obsolete version constraints of dependencies.
    - Update team name.
  * debian/patches:
    - Adjust activerecord patch to version 6, and drop obsolete migration
      specs. (Closes: #964143)
    - Reintroduce patch to disallow the use of Tor and to enforce the use of
      the so-called "standard resolver", if running the specs and executing
      dirmngr. This was dropped in version 3.5.3-1, however, it seems, it is
      still required.
    - Refresh remaining patches.

 -- Georg Faerber <georg@debian.org>  Sun, 27 Dec 2020 19:15:38 +0000

schleuder (3.5.3-1) unstable; urgency=medium

  * New upstream version:
    - Ships fix to allow specs to run on IPv6-only machines. (Closes: #962585)
  * debian/patches:
    - Drop patch to disallow the use of Tor and to enforce the use of the
      so-called "standard resolver", if running the specs and executing
      dirmngr. This was required in the past to make dirmngr work reliably. It
      seems this is obsolete by now, however, real-world exposure on the
      Debian buildd and CI infrastructure might prove different.
    - Refresh for new release.

 -- Georg Faerber <georg@debian.org>  Sat, 13 Jun 2020 14:18:05 +0000

schleuder (3.5.2-2) unstable; urgency=medium

  * debian/tests/schleuder-api-daemon-reachable:
    - Let curl timeout after five seconds.
    - Add missing square brackets to ::1 address.
    - Fix exit code handling.

 -- Georg Faerber <georg@debian.org>  Wed, 10 Jun 2020 11:16:37 +0000

schleuder (3.5.2-1) unstable; urgency=medium

  * New upstream version 3.5.2:
    - Ships fix to let x-add-key handle a mix of inline key and non-key
      material, like a signature included in the body. (Closes: #961017)
  * debian/tests/schleuder-api-daemon-reachable:
    - Let test pass if the connection to either 127.0.0.1 or to ::1 works and
      returns the expected result. During some runs, Schleuder would only bind
      to the later. Up until now, the test only checked the former. See commit
      782a6f1c198b0fd1746ecff72860071ed80adc57 for more details.

 -- Georg Faerber <georg@debian.org>  Tue, 09 Jun 2020 23:09:41 +0000

schleuder (3.5.1-1) unstable; urgency=medium

  * New upstream version 3.5.1:
    - Ships fix for x-add-key to recognize mails with attached,
      quoted-printable encoded keys. Such mails might be produced by
      Thunderbird. (Closes: #956827)
  * debian/patches:
    - Drop obsolete patch to fix flaky migration spec, merged upstream.
    - Refresh for new release.

 -- Georg Faerber <georg@debian.org>  Wed, 15 Apr 2020 18:25:25 +0000

schleuder (3.5.0-3) unstable; urgency=medium

  * debian/control:
    - Update description according to upstream.
    - Make dependency on ruby-mail-gpg more strict: the most recent upstream
      version 0.4.3 ships breaking changes.

 -- Georg Faerber <georg@debian.org>  Thu, 09 Apr 2020 19:19:47 +0000

schleuder (3.5.0-2) unstable; urgency=medium

  * debian/patches:
    - Fix patch to relax dependency on bigdecimal in the gemspec. This change
      fixes a regression introduced via the previous upload, 3.5.0-1, which
      blocked the migration to testing, as the version provided there is still
      1.3.4. Initially, the patch allowed all versions >= 1.4.

 -- Georg Faerber <georg@debian.org>  Thu, 02 Apr 2020 15:01:34 +0000

schleuder (3.5.0-1) unstable; urgency=medium

  * New upstream version 3.5.0.
  * debian/control:
    (Build-)Depend on ruby-charlock-holmes.
  * debian/patches:
    - Drop obsolete patches to add List-Id header to admin notifications, to
      handle decryption problems and to default to ASCII-8BIT encoding, merged
      upstream.
    - Refresh remaining patches for new release.
    - Add patch to relax dependency on bigdecimal in gemspec.
    - Add patch to fix flaky migration spec.

 -- Georg Faerber <georg@debian.org>  Tue, 31 Mar 2020 23:50:14 +0000

schleuder (3.4.1-4) unstable; urgency=medium

  * debian/*:
    - Run wrap-and-sort -abt.
  * debian/patches:
    - Relax dependency on sqlite3 in gemspec. This fixes a regression
      introduced in the previous release, 3.4.1-3. (Closes: #951225)
    - Refresh existing patches, accordingly.

 -- Georg Faerber <georg@debian.org>  Thu, 13 Feb 2020 22:59:08 +0000

schleuder (3.4.1-3) unstable; urgency=medium

  * debian/control:
    - Bump Standards-Version to 4.5.0, no changes required.
  * debian/patches:
    - Add patch to bump SQLite dependency in gemspec. (Closes: #949824)
    - Refresh patch to update sinatra to remove fuzz.

 -- Georg Faerber <georg@debian.org>  Mon, 03 Feb 2020 11:07:21 +0000

schleuder (3.4.1-2) unstable; urgency=medium

  * debian/control:
    - Bump Standards-Version to 4.4.1, no changes required.
  * debian/copyright:
    - Bump years to include 2020.
  * debian/patches:
    - Pull in upstream patch to add missing List-Id header to notification
      mails sent to admins. (Closes: #948980)
    - Pull in upstream patch to handle decryption problems gracefully: Handle
      incoming mails encrypted to an absent key, using symmetric encryption or
      containing PGP-garbage in a more graceful manner: Don't throw an
      exception, don't notify (and annoy) the admins, instead inform the
      sender of the mail how to do better. (Closes: #948981)
    - Pull in upstream patch to default to ASCII-8BIT encoding. This should
      ensure Schleuder is able to handle mails with different charsets.
      (Closes: #948982)

 -- Georg Faerber <georg@debian.org>  Wed, 15 Jan 2020 16:38:14 +0000

schleuder (3.4.1-1) unstable; urgency=medium

  * New upstream version 3.4.1:
    - Fixes recognizing keywords in mails with protected headers and empty
      subject. (Closes: #940524)
    - Drops third-party signatures on keys before importing to prevent
      signature flooding. (Closes: #940526)
    - Reports an error if the argument provided to 'refresh_keys' is not an
      email address for which a list exists. (Closes: #940527)
  * debian/patches:
    - Drop patch to fix mails produced by Mutt with protected headers,
      released upstream.

 -- Georg Faerber <georg@debian.org>  Mon, 16 Sep 2019 21:09:53 +0000

schleuder (3.4.0-4) unstable; urgency=medium

  * debian/salsa-ci.yml:
    - Move CI config according to team policy.
    - Disable reprotest job, as it leads to false positives.
  * debian/tests/upstream-tests:
    - Drop relying on /dev/urandom if running the test suite in a container.
      This was introduced two years ago, to deal with limited entropy, which
      would lead to the test suite timing out, if invoked on Debian's
      autopkgtest infrastructure running on ci.debian.net.
      As noted by Gianfranco Costamagna in #939630, the specifics of this
      workaround lead to problems on armhf during tests run by Ubuntu's
      autopkgtest infrastructure.
      This change is experimental, some real world tests need to be done to
      check if the underlying problem still exist. In case, a more portable
      approach needs to be found.

 -- Georg Faerber <georg@debian.org>  Sat, 07 Sep 2019 11:39:23 +0000

schleuder (3.4.0-3) unstable; urgency=medium

  * debian/control:
    - Bump debhelper compat level to 12.
    - Bump Standards-Version to 4.4.0, no changes required.
    - Add missing Pre-Depends field to make lintian happy.
    - Use my debian.org email address.
  * debian/schleuder.schleuder-api-daemon.init:
    - Fix file name; previously, dh_installinit would fail.
  * debian/.gitlab-ci.yml:
    - Import breaking upstream changes, made by the Salsa CI team, to fix
      errors.

 -- Georg Faerber <georg@debian.org>  Wed, 24 Jul 2019 00:42:26 +0000

schleuder (3.4.0-2) unstable; urgency=medium

  * debian/patches:
    - Pull in upstream patch to handle mails with protected headers as
      introduced in Mutt 1.12.0, which was recently released. These headers
      are just contained within the plain body of a mail produced by Mutt,
      they are not further wrapped into a specifically marked MIME-part.
      Schleuder fails to handle such messages, accordingly, this patch fixes
      this behaviour. (Closes: #930870)

 -- Georg Faerber <georg@debian.org>  Fri, 21 Jun 2019 19:05:42 +0000

schleuder (3.4.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/changelog:
    - Drop trailing whitespaces.
  * debian/patches:
    - Drop nearly all, now obsolete, patches newly applied in the -7 to -10
      releases. All of them are integrated upstream, except for the patch
      which bumps the dependencies on sinatra and sinatra-contrib to ~> 2, as
      defined in the gemspec, and a patch which adapts some parts of the code
      to ruby-activerecord 5.2 as shipped in unstable and testing.
    - Refresh for new release.
  * debian/tests:
    - Rename test which checks if the API daemon is reachable after
      installation to better reflect reality.
  * debian/upstream/signing-key.asc:
    - Remove unneeded signatures.

 -- Georg Faerber <georg@riseup.net>  Thu, 14 Feb 2019 17:10:34 +0000

schleuder (3.3.0-10) unstable; urgency=medium

  * debian/control:
    - Bump build dependency on ruby-database-cleaner to >= 1.7.0~.
  * debian/patches:
    - Pull in upstream patch to not rely on relative paths in some specs.
      Loading the filters by relative paths doesn't work in the context of
      autopkgtest. In the past, this made the testsuite unreliable, as the
      success depended on the execution order of the specs.
    - Drop patch to disable flaky spec which tests signed-inline messages, to
      re-enable it.
    - Pull in upstream patch which reworks this spec to make it more reliable.
    - Unify two patches to adapt the gemspec and the specs to ruby-mail 2.7.1.
    - Refresh for new release.

 -- Georg Faerber <georg@riseup.net>  Tue, 12 Feb 2019 23:41:27 +0000

schleuder (3.3.0-9) unstable; urgency=medium

  * debian/control:
    - Drop obsolete dependency on debhelper. This is satisfied since a while
      by a build dependency on debhelper-compat.
  * debian/Rakefile:
    - Require active_record first before loading active_record code. This
      change is necessary to adapt to a recent change in rails.

 -- Georg Faerber <georg@riseup.net>  Thu, 07 Feb 2019 20:59:48 +0000

schleuder (3.3.0-8) unstable; urgency=medium

  * debian/control:
    - Bump required versions of ruby-sinatra and ruby-sinatra-contrib to
      >= 2~.
    - Add missing dependencies on ruby-mail >= 2.7.1~.
  * debian/patches:
    - Pull in upstream patch to strip HTML-part of
      multipart/alternative-messages if it contains keywords. The keywords
      aren't parsed, because Schleuder doesn't touch any HTML. In order to
      prevent the keywords from being disclosed to third parties, for example
      via resent messages, the HTML-part is completely dropped. This issue
      should be considered privacy-sensitive.
    - Add patch to disable a flaky spec, which tests signed-inline cleartext
      messages. Since some time, this formerly pretty stable spec became
      flaky. It it still unclear what causes this, ruby-mail 2.7.1 might be
      somehow related. (Closes: #919072)
    - Add patch which fixes the expected output of one spec. This spec tests
      that mails with broken UTF-8 don't raise an error. However, the fixture
      mail is empty. Schleuder rightfully doesn't deliver the mail, but
      reports it as empty. This problem became only obvious by now, using
      ruby-mail 2.7.1.
    - Add patch to bump the dependency on the mail gem, as both unstable and
      testing ship a newer major version. (Closes: #920031)
    - Add patch to bump the dependency on the sinatra and sinatra-contrib
      gems, as both unstable and testing ship newer major versions.
      This patch, and the three before, should fix the current FTBFS
      situation, and allow Schleuder to migrate to testing before the upcoming
      soft freeze.
    - Add missing .patch extension to one patch.
    - Refresh for new release.

 -- Georg Faerber <georg@riseup.net>  Tue, 05 Feb 2019 22:32:16 +0000

schleuder (3.3.0-7) unstable; urgency=medium

  * debian/control:
    - Bump dependencies on ruby-activerecord to >= 5.2~.
    - Add build dependency on ruby-rack-test. This is needed due to
      ruby-sinatra >= 2 in use.
    - Bump Standards-Version to 4.3.0, no changes necessary.
  * debian/copyright:
    - Bump years to include 2019.
  * debian/patches:
    - Pull in upstream patch to fix the expected output of two specs:
      ruby-mail 2.7.1 inserts \r additionally to \n, to mark newlines.
      (Closes: #919072)
    - Pull in upstream patch including fixes for ruby-activerecord 5.2: it
      specifies the ActiveRecord version in each migration, and adjusts the
      migrations spec to the new ActiveRecord::Migrator API.
      (Closes: #918569)
  * debian/tests/upstream-tests:
    - Check the environment, before linking /dev/random to /dev/urandom to
      deal with limited entropy, to only execute this workaround in
      environments other than the GitLab CI one. This change was necessary, to
      fix the otherwise failing autopkgtest job provided by the Salsa CI team
      due to the filesystem being read-only.
  * debian/.gitlab-ci.yml:
    - Drop custom config, rely on the Salsa CI Team.

 -- Georg Faerber <georg@riseup.net>  Mon, 04 Feb 2019 02:15:24 +0000

schleuder (3.3.0-6) unstable; urgency=medium

  * debian/changelog:
    - Fix typos.
  * debian/control:
    - Fix typo.
  * debian/patches:
    - Reintroduce dirmngr patch to disable Tor and enforce the use of the
      "standard resolver" while running the specs. It seems, that dirmngr
      still doesn't work reliably if invoked in a chroot.

 -- Georg Faerber <georg@riseup.net>  Fri, 26 Oct 2018 11:29:46 +0000

schleuder (3.3.0-5) unstable; urgency=medium

  * debian/compat:
    - Drop obsolete compat file, now handled via build dependency.
  * debian/control:
    - Add debhelper-compat as a build dependency.
    - Declare that the build doesn't need root privileges.
  * debian/patches:
    - Drop patch, which added a dirmngr config to disable the use of Tor and
      enforced the use of the so called "standard resolver" for DNS
      resolution. Older dirmngr versions were problematic in the past in this
      regard, and lead to failing specs. Further observation is needed, to
      ensure the situation changed for the better.

 -- Georg Faerber <georg@riseup.net>  Thu, 25 Oct 2018 21:49:49 +0000

schleuder (3.3.0-4) unstable; urgency=medium

  * debian/schleuder.postinst:
    - Handle upgrade from schleuder < 3.0. (Closes: #867031)
  * debian/rules:
    - Make the build verbose.
  * debian/.gitlab-ci.yml:
    - Install and start haveged, drop linking /dev/random to /dev/urandom. A
      recent change in the CI infrastructure of salsa.d.o made this necessary,
      as the former solution failed due to "read-only file system".

 -- Georg Faerber <georg@riseup.net>  Thu, 04 Oct 2018 10:07:21 +0000

schleuder (3.3.0-3) unstable; urgency=medium

  * debian/watch:
    - Ensure that the version group starts with a digit. Otherwise, uscan
      might try to pull in the schleuder-cli release tarball.

 -- Georg Faerber <georg@riseup.net>  Tue, 02 Oct 2018 09:07:06 +0000

schleuder (3.3.0-2) unstable; urgency=medium

  * debian/schleuder.postinst:
    - Fix permissions of the config which sets default values for new lists.

 -- Georg Faerber <georg@riseup.net>  Thu, 27 Sep 2018 22:52:57 +0000

schleuder (3.3.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/changelog:
    - Fix typos.
  * debian/copyright:
    - Use team@schleuder.org as upstream contact email.
    - Use HTTPS in link to GPL version 3.
    - Wrap and sort.
  * debian/control:
    - Bump Standards-Version to 4.2.1, no changes needed.
    - Use schleuder.org as upstream homepage.
    - Depend on ruby-factory-bot, instead of ruby-factory-girl, while
      building.
    - Require ruby-mail-gpg >= 0.3.3~. (Closes: #901168)
    - Require default-mta or postfix or mail-transport-agent.
    - Wrap and sort.
  * debian/patches:
    - Add patch to remove rack-test dependency, as we are not dealing with
      ruby 2.1.
    - Drop obsolete patch to fix paths, applied upstream.
    - Drop obsolete patch to bump attempts to start SKS mock, fixed upstream.
    - Refresh for new release.
  * debian/schleuder.postinst:
    - Run 'schleuder install' via 'runuser' as the schleuder user.
    - Change ownership of the '/etc/schleuder' directory to the schleuder user,
      to ensure TLS certificates can be written there.
    - Drop obsolete parts which fixed permissions and ownership of the
      SQLite database and the certificates. Instead, rely on 'schleuder
      install' to do the right thing.
    - Make configs writable by the root user, and readable by the schleuder
      user.
    - Drop 'find', introduced in the last release, in an useless attempt to
      overcome the security issues related to recursive chmod or chown.
      Instead, specific filenames and directories are used if invoking either
      chmod or chown. Thanks a lot, dkg!
    - Drop obsolete version check, which compared the to-be-installed version
      against the already installed version.
    - Finally, fix typos and intendation.
  * debian/schleuder.preinst:
    - Handle the backup directory exclusively via this file, if upgrading from
      schleuder < 3.0. Before, permissions were fixed in the 'postinst' stage.
    - Also, while at it, fix intendation.
  * debian/ruby-tests.rake:
    - Add environment variable to sleep in between SKS mock start attempts.
  * debian/schleuder.dirs:
    - Create the '/usr/local/lib/schleuder/filters' directory to enable users
      to provide their own custom filters.
    - Drop obsolete directories.
    - Wrap and sort.
  * debian/schleuder.install:
    - Wrap and sort.
  * debian/schleuder.manpages:
    - Wrap and sort.
  * debian/schleuder.schleuder-api-daemon.service:
    - Point to documentation at schleuder.org.
  * debian/watch:
    - Use schleuder.org to check for new versions.
  * debian/.gitlab-ci.yml:
    - Introduce configuration to make use of continuous integration as
      provided by salsa.debian.org.

 -- Georg Faerber <georg@riseup.net>  Thu, 27 Sep 2018 15:58:48 +0000

schleuder (3.2.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/changelog: Remove trailing whitespaces.
  * debian/compat: Bump debhelper compat level to 11.
  * debian/control:
    - Bump Standards-Version to 4.1.3, no changes needed.
    - Use salsa.debian.org in Vcs-Browser and Vcs-Git fields, as
      anonscm.debian.org is deprecated and accordingly, all repositories were
      moved.
    - Bump required debhelper version to 11~.
    - Add the propcs package as a build dependency, as some specs use
      'pgrep'.
  * debian/copyright: Bump years to include 2018 and use HTTPS in link to
    copyright format specification.
  * debian/patches:
    - Refresh for release 3.2.2-1.
    - Add patch to remove the spec which tests and expects the install to
      fail, if a shell-process fails, as it fails itself, currently. The
      discussion with upstream how to solve this in a sane way is ongoing.
    - Add patch to increase attempts to start the SKS mock keyserver which is
      used for running the specs, and the time to sleep between each attempt.
      Sometimes, the SKS mock keyserver didn't start within the given time,
      leading to failed specs. This will be fixed upstream and included in the
      next release.
    - Add patch to disable Tor and to enforce the use of the so called
      "standard resolver" for DNS resolution in dirmngr. It seems, dirmngr
      still sometimes has problems to connect to a keyserver if invoked in a
      chroot, leading to failed specs. This patch only affects the dirmngr
      configuration related to running the specs.
  * debian/Rakefile: Add new alias for database setup as used by upstream.
  * debian/{rules,s-a-d.service}: In debhelper compat level 11, the systemd
    sequence for dh was removed, and the dh_systemd_{enable,start,stop}
    helpers as well. Accordingly, drop the obsolete helpers and make use of
    the new dh_installsystemd helper.
    Moreover, if specifying '--name' for dh_installsystemd, debhelper now
    expects the systemd service file to be installed to be named as
    $package-name.$service-name.service. Accordingly, rename the service file
    of the API daemon.
  * debian/schleuder.postinst: Fix permissions of files and directories. A
    user reported upstream, that the files inside '/etc/schleuder' had the
    executable bit set.
    At the same time it was reported in bugs #889060 and #889488, that doing
    recursive chmod or chown might lead to root escalation on systems with
    'fs.protected_hardlinks=0'.
    This change fixes both problems via using 'find'. Additionally, the
    permissions are made more strict, to ensure that the directories and
    files are only accessible by the schleuder user. Before, the schleuder
    group was allowed access, too.
  * debian/watch: Update for new upstream release download location.

 -- Georg Faerber <georg@riseup.net>  Tue, 13 Feb 2018 03:24:52 +0100

schleuder (3.2.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Drop unnecessary dependency on init-system-helpers, as this is an
      essential package.
    - Explicitly depend on ruby-gpgme >= 2.0.13~: Up until 2.0.12, ruby-gpgme
      suffered from a bug which lead to false constructed key capabilities. In
      2013, the bug was found and a patch was written, but it took three more
      years for the patch to be included in a release.
    - Relax required rake version. This helps with backporting schleuder to
      stretch, and ensures a minimal delta between buster and stretch.
  * debian/NEWS: Add note about how to rely on the Debian keyserver defaults.
  * debian/patches:
    - Add patch to rely on the Debian keyserver defaults in the schleuder
      config. (Closes: #852710)
    - Drop patch to disable bundler, integrated upstream.
    - Refresh for release 3.2.1-1.
  * debian/ruby-tests.rake:
    - Disable bundler and test coverage check.
    - Don't kill gpg-agent before and after running the tests, integrated
      upstream.
    - Use random path for database and lists directory to prevent known
      tmpfile attacks.
  * debian/tests:
    - Explain why we aren't using haveged anymore, and instead, why we link
      /dev/random to /dev/urandom. Please, don't do this in production!
    - Accordingly, drop obsolete dependency on haveged.
    - Checking the status of the API is now possible without authentication.
      Therefore, update the expected response if querying the API status
      during autopkgtest.

 -- Georg Faerber <georg@riseup.net>  Fri, 27 Oct 2017 18:42:56 +0200

schleuder (3.1.2-3) unstable; urgency=medium

  * debian/tests/upstream-tests: Drop starting haveged manually, because
    this doesn't work anymore, if invoked in a container. Instead, link
    /dev/random to /dev/urandom.
  * debian/ruby-tests.rake: Kill gpg-agent before and after running tests.
    This might help with making schleuder build reproducibly.

 -- Georg Faerber <georg@riseup.net>  Sat, 30 Sep 2017 14:21:38 +0200

schleuder (3.1.2-2) unstable; urgency=medium

  * debian/control:
    - Bump Standards-Version to 4.1.1.0, no changes needed.
    - Drop obsolete build dependency on 'dh-systemd'.
    - Drop obsolete 'Testsuite', this is added automatically by dpkg-source
      since dpkg 1.17.1, if a debian/tests/control file exists.

 -- Georg Faerber <georg@riseup.net>  Sat, 30 Sep 2017 09:05:32 +0200

schleuder (3.1.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright:
    - Bump years to include 2017.
    - Fix upstream source location.
  * debian/control: Bump required rake version to >= 12~.
  * debian/schleuder.preinst:
    - Check for existing schleuder < 3.0 data first, before doing the backup.
    (Closes: #867031)
    - Don't use 'echo' to inform the user about the data migration process,
    but instead via a proper NEWS file.
  * debian/NEWS: Add said file with notes on how to migrate schleuder < 3.0
    data.
  * debian/patches/0003-bin-fix-require.patch: Refresh for 3.1.2-1.

 -- Georg Faerber <georg@riseup.net>  Thu, 13 Jul 2017 13:40:47 +0200

schleuder (3.1.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches/0001-lib-fix-paths.patch: Refresh for new release.

 -- Georg Faerber <georg@riseup.net>  Sun, 25 Jun 2017 14:58:01 +0200

schleuder (3.1.0-3) unstable; urgency=medium

  * debian/rules:
    - Install SysV init script.
    - Drop unneeded debhelper '--no-start' overrides; debhelper should
      enable and start the Schleuder API daemon.
  * debian/control:
    - Depend on lsb-base package for init-functions.
    - Fix (Build-)Depends: formatting.
    - Bump Standards-Version to 4.0.0.
  * debian/schleuder-api-daemon.init: Rework SysVinit script: Fix formatting
    and styling, add required and recommend commands status, reload,
    force-reload by Lintian.
  * debian/schleuder.cron.weekly: Refresh keys first, ignore errors and don't
    exit early. (Closes: #853836)
  * debian/schleuder.preinst: If upgrading from schleuder 2, move data to
    separate backup dir and inform the user. (Closes: #853841)
  * debian/schleuder.postinst:
    - Remove obsolete deb-systemd-helper calls, this is handled by debhelper
      now.
    - Ensure sane permissions of backup dir. (Closes: #853841)
  * debian/schleuder.postrm: Remove the backup dir if purging the package.
    (Closes: #853841)
  * debian/README.Debian: Add notes to migrate lists from schleuder 2 to 3.
    (Closes: #853841)
  * debian/patches: As data is now moved to a separate backup dir if upgrading
    from schleuder 2, drop the obsolete patching of 'schleuder install', which
    disabled the check for existing data. (Closes: #853841)

 -- Georg Faerber <georg@riseup.net>  Tue, 20 Jun 2017 13:17:00 +0200

schleuder (3.1.0-2) unstable; urgency=medium

  * debian/tests/upstream-tests: Ensure haveged is running before executing
    the upstream provided tests, which rely heavily on entropy.
  * debian/tests/control: Add 'needs-root' restriction to actually be able to
    start haveged manually. The provided init script disables the automatic
    start in containerized environments.

 -- Georg Faerber <georg@riseup.net>  Tue, 13 Jun 2017 00:28:23 +0200

schleuder (3.1.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/gbp.conf: Removed, not needed anymore.
  * debian/watch: Fixed, broken due to Gitlab upgrade.

 -- Georg Faerber <georg@riseup.net>  Sat, 10 Jun 2017 19:37:40 +0200

schleuder (3.0.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/schleuder.cron.weekly: Refresh keys from the keyservers.
  * debian/schleuder.{dirs,install}: Ship Postfix config file.
  * debian/patches/: Refresh for Debian schleuder release 3.0.1-1.
  * debian/control: Add dirmngr to {Build-}Depends:.
  * debian/tests/check-status: Rework check against installed API daemon. This
    is needed because of the recent upstream changes which made TLS and API
    keys mandatory.

 -- Georg Faerber <georg@riseup.net>  Thu, 26 Jan 2017 12:33:06 +0100

schleuder (3.0.0~beta17-2) unstable; urgency=medium

  * debian/control: Depend on cron or cron-daemon. (Closes: #851732)
  * debian/schleuder.postinst: Don't 'touch' database; fixed upstream.
  * debian/tests/control: Install haveged to help with entropy.
  * debian/watch: Watch 0xacab.org; the upstream repository moved.
  * debian/ruby-tests.rake: Fix and call cleanup task.

 -- Georg Faerber <georg@riseup.net>  Wed, 25 Jan 2017 15:27:28 +0100

schleuder (3.0.0~beta17-1) unstable; urgency=medium

  [ Georg Faerber ]
  * New upstream release. (Closes: #850545)
  * debian/schleuder-api-daemon.init: Add sysvinit script.
  * debian/patches: Refresh for Debian schleuder release 3.0.0~beta17-1.
  * debian/ruby-tests.rake:
    - Cover all upstream tests.
    - Remove test database to make the build reproducible.

  [ Sebastien Badia ]
  * debian/control:
    - Wrap and sort.
    - Add ruby-factory-girl as Build-Depends.
  * debian/compat: Bump debhelper compat to 10.

 -- Georg Faerber <georg@riseup.net>  Thu, 12 Jan 2017 19:52:43 +0100

schleuder (3.0.0~beta11-2) unstable; urgency=medium

  * debian/schleuder.postinst: Restart the API daemon by default
  * debian/ruby-tests.rake: Load correct Rakefile and ensure clean
    environment

 -- Georg Faerber <georg@riseup.net>  Tue, 13 Dec 2016 18:56:32 +0100

schleuder (3.0.0~beta11-1) unstable; urgency=medium

  * Initial release (Closes: #845636)

 -- Georg Faerber <georg@riseup.net>  Mon, 12 Dec 2016 16:20:50 +0100
